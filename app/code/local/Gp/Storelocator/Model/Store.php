<?php
/**
 * Created by PhpStorm.
 * User: gianluca.porta
 * Date: 02/10/15
 * Time: 09:48
 */
class Gp_Storelocator_Model_Store extends Mage_Core_Model_Abstract{
    protected function _construct(){
        $this->_init('storelocator/store');
    }
    public function getStoreJson(){
        $storeLocatoreCollection = Mage::getResourceModel('storelocator/store_collection')->addFieldToFilter('active',1);
        foreach($storeLocatoreCollection as $store){
            $json[] = array('name'=>$store->getNome(),'indirizzo'=>$store->getIndirizzo(),'latitudine'=>$store->getLatitudine(),'longitudine'=>$store->getLongitudine());
        }
        return json_encode($json);
    }
}
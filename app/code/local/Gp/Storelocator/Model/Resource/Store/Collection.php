<?php
/**
 * Created by PhpStorm.
 * User: gianluca.porta
 * Date: 02/10/15
 * Time: 09:48
 */
class Gp_Storelocator_Model_Resource_Store_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract{
    public function _construct() {
        parent::_construct();
        $this->_init('storelocator/store');
    }
}
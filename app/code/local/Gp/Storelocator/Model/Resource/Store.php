<?php
/**
 * Created by PhpStorm.
 * User: gianluca.porta
 * Date: 02/10/15
 * Time: 09:48
 */
class Gp_Storelocator_Model_Resource_Store extends Mage_Core_Model_Resource_Db_Abstract{
    protected function _construct(){
        $this->_init('storelocator/store', 'id');
    }
}
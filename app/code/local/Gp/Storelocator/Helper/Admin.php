<?php
/**
 * Created by PhpStorm.
 * User: gianlucaporta
 * Date: 09/10/14
 * Time: 11:45
 */
class Gp_Storelocator_Helper_Admin extends Mage_Core_Helper_Abstract{
    public $resource;
    public $structure;

    public function __construct(){
        $this->resource = Mage::getSingleton('core/resource');
        $this->structure = $this->resource->getConnection('core_read')->fetchAll("SHOW COLUMNS FROM ".$this->resource->getTableName('storelocator/store'));
    }
    public function getStructureTable(){

    }
    public function getExcludeColumnGrid(){
        return array();
    }
    public function getExcludeFieldForm(){
        return array('id');
    }
    public function getFormInfo(){
        $excludeField = $this->getExcludeFieldForm();
        $info = array();
        foreach($this->structure as $column){
            $name = str_replace(' ','-',$column['Field']);
            if (in_array($name, $excludeField))
                continue;
            switch($name){
                case 'active':
                    $info[$name]['type']   = 'select';
                    $info[$name]['required'] = true;
                    $info[$name]['values'] = $this->getYesNo();
                    break;
                default:
                    $info[$name]['type'] = 'text';
                    $info[$name]['required'] = true;
                    break;
            }
        }
        return $info;
    }
    public function getColumnInfo(){
        $excludeColumn = $this->getExcludeColumnGrid();
        $info = array();
        foreach($this->structure as $column){
            $name = str_replace(' ','-',$column['Field']);
            if (in_array($name, $excludeColumn))
                continue;
            switch($name){
                case 'id':
                    $info[$name]['type']   = 'number';
                    break;
                case 'active':
                    $info[$name]['type'] = 'options';
                    $info[$name]['options'] = $this->getYesNo();
                    break;
                default:
                    $info[$name]['type'] = 'text';
                    break;
            }
        }
        return $info;
    }
    protected function typeIsRequired(){
        return array(0,1);
    }
    protected function typeStorelocator(){
        return array(array('label'=>'Select','value'=>''),array('label'=>'Text','value'=>'text'));
    }
    protected function typeLocation(){
        return array(array('label'=>'Select','value'=>''),array('label'=>'Page','value'=>'page'));
    }
    protected function getYesNo(){
        return array('No','Si');
    }
}
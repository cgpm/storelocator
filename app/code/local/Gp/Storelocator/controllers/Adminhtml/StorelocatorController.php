<?php
/**
 * Created by PhpStorm.
 * User: gianlucaporta
 * Date: 09/10/14
 * Time: 12:14
 */
class Gp_Storelocator_Adminhtml_StorelocatorController extends Mage_Adminhtml_Controller_action{
    public function _construct(){
        parent::_construct();
    }

    protected function _isAllowed(){
        return true;
    }

    protected function _initAction(){
        $this->loadLayout()->_setActiveMenu("storelocator/storelocator");
        $this->_addContent($this->getLayout()->createBlock('storelocator/adminhtml_storelocator'));
        return $this;
    }
    public function indexAction(){
        $this->_title($this->__("Storelocator"));
        $this->_title($this->__("Manager Storelocator"));

        $this->_initAction();
        $this->renderLayout();
    }
    public function newAction(){
        return $this->_initAction()->_addContent($this->getLayout()->createBlock('storelocator/adminhtml_add'))->renderLayout();
    }
    public function saveAction(){
        $storelocatorModel = Mage::getModel('storelocator/store');
        if($this->getRequest()->getParam('id'))
            $field = $storelocatorModel->load($this->getRequest()->getParam('id'));
        else
            $field = $storelocatorModel;

        $postData = $this->getRequest()->getPost();
        foreach ($postData as $k => $v){
            if($k=='form_key')
                continue;
            $field->setData($k,$v);
        }
        $field->save();
        Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('core')->__('Operazione terminata correttamente.'));
        if ($this->getRequest()->getParam('back'))
            return $this->_redirect('*/*/edit', array('id' => $field->getId()));
        return $this->_redirect('*/*/');
    }
    public function deleteAction(){
        $field = Mage::getModel('storelocator/store')->load($this->getRequest()->getParam('id'));
        $field->delete();
        Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('core')->__('Operazione terminata correttamente.'));
        $this->_redirect('*/*/');
    }
    public function editAction(){
        $this->_title($this->__("Storelocator"));
        $this->_title($this->__("Manager Storelocator"));

        $this->loadLayout()->_setActiveMenu("storelocator/storelocator");
        $this->_addContent($this->getLayout()->createBlock('storelocator/adminhtml_storelocator_edit'));
        $this->renderLayout();
    }
    public function massDeleteAction(){
        $fieldIds = $this->getRequest()->getParam('store');

        if (!is_array($fieldIds))
            $this->_getSession()->addError($this->__('Please select field(s).'));
        else {
            if (!empty($fieldIds)) {
                try {
                    foreach ($fieldIds as $fieldId) {
                        $field = Mage::getModel('storelocator/store')->load($fieldId);
                        $field->delete();
                    }
                    $this->_getSession()->addSuccess(
                        $this->__('Total of %d record(s) have been deleted.', count($fieldIds))
                    );
                }catch (Exception $e){
                    $this->_getSession()->addError($e->getMessage());
                }
            }
        }
        $this->_redirect('*/*/index');
    }
}
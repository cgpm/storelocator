<?php

/**
 * Created by PhpStorm.
 * User: gianlucaporta
 * Date: 09/10/14
 * Time: 16:41
 */

class Gp_Storelocator_Block_Adminhtml_Storelocator extends Mage_Adminhtml_Block_Widget_Grid_Container{
    public function __construct(){
        $this->_controller = "adminhtml_storelocator";
        $this->_blockGroup = "storelocator";
        $this->_headerText = Mage::helper("storelocator")->__("Storelocator Manager");
        $this->_addButtonLabel = Mage::helper("storelocator")->__("Add New Item");
        parent::__construct();
    }

    public function getCreateUrl(){
        return $this->getUrl('storelocator/adminhtml_storelocator/edit');
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: gianlucaporta
 * Date: 16/10/14
 * Time: 10:37
 */

class Gp_Storelocator_Block_Adminhtml_Storelocator_Edit extends Mage_Adminhtml_Block_Widget_Form_Container{
    public function __construct(){
        $this->_objectId = 'id';
        $this->_blockGroup = 'storelocator';
        $this->_controller = 'adminhtml_storelocator';

        $this->_addButton('save_and_continue', array(
            'label' => Mage::helper('storelocator')->__('Save And Continue Edit'),
            'onclick' => 'saveAndContinueEdit()',
            'class' => 'save'
        ), -100);

        $this->_formScripts[] = "
             function saveAndContinueEdit(){
                editForm.submit($('edit_form').action + 'back/edit/');
             }";
        $this->setId('id');
        parent::__construct();
    }
}
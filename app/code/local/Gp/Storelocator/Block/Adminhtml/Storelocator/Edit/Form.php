<?php
/**
 * Created by PhpStorm.
 * User: gianlucaporta
 * Date: 09/10/14
 * Time: 12:23
 */
class Gp_Storelocator_Block_Adminhtml_Storelocator_Edit_Form extends Mage_Adminhtml_Block_Widget_Form{
    public function _construct(){
        parent::_construct();
    }
    protected function _prepareForm(){
        $form = new Varien_Data_Form(array(
            'id'      => 'edit_form',
            'action'  => $this->getUrl('*/*/save',array('id' => $this->getRequest()->getParam('id'))),
            'method'  => 'post',
            'enctype' => 'multipart/form-data'
        ));
        $fieldset = $form->addFieldset('Add', array('legend' => 'Add'));

        $helper = Mage::helper('storelocator/admin');
        $infoForm = $helper->getFormInfo();
        $excludeField = $helper->getExcludeFieldForm();

        $structure = $helper->structure;
        foreach($structure as $column){
            $name = str_replace(' ','-',$column['Field']);
            if (in_array($name, $excludeField))
                continue;
            $config = $infoForm[$name];
            $config['label'] = $column['Field'];
            $config['name'] = $name;

            if(isset($infoForm[$name]['values']) && !empty($infoForm[$name]['values']))
                $config['values'] = $infoForm[$name]['values'];

            $fieldset->addField($name,$infoForm[$name]['type'],$config);
        }
        $form->setUseContainer(true);
        if($this->getRequest()->getParam('id'))
            $form->setValues(Mage::getModel('storelocator/store')->load($this->getRequest()->getParam('id')));
        $this->setForm($form);
        return parent::_prepareForm();
    }
}
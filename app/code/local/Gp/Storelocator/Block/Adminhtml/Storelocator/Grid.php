<?php
/**
 * Created by PhpStorm.
 * User: gianlucaporta
 * Date: 16/10/14
 * Time: 10:32
 */
class Gp_Storelocator_Block_Adminhtml_Storelocator_Grid extends Mage_Adminhtml_Block_Widget_Grid{
    public function _construct(){
        parent::_construct();
        $this->setId('storelocator');
        $this->setDefaultSort('id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }
    protected function _prepareCollection(){
        $this->setCollection(Mage::getModel('storelocator/store')->getCollection());
        return parent::_prepareCollection();
    }

    protected function _prepareColumns(){
        $helper = Mage::helper('storelocator/admin');
        $excludeColumn = $helper->getExcludeColumnGrid();
        $infoColumn = $helper->getColumnInfo();
        $structure = $helper->structure;
        foreach($structure as $column){
            $name = str_replace(' ','-',$column['Field']);
            if(in_array($name, $excludeColumn))
                continue;
            $config = $infoColumn[$name];
            $config['header'] = ucwords(str_replace('_',' ',$column['Field']));
            $config['index'] = $name;
            $config['align'] = 'center';

            if(isset($infoColumn[$name]['values']) && !empty($infoColumn[$name]['values']))
                $config['values'] = $infoColumn[$name]['values'];

            $this->addColumn($name, $config);
        }
        return parent::_prepareColumns();
    }

    protected function _prepareMassaction(){
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('store');

        $this->getMassactionBlock()->addItem('delete', array(
            'label'=> Mage::helper('tax')->__('Delete'),
            'url'  => $this->getUrl('*/*/massDelete'),
            'confirm' => Mage::helper('storelocator')->__('Are you sure?')
        ));
        return $this;
    }

    public function getRowUrl($row){
        return $this->getUrl('storelocator/adminhtml_storelocator/edit', array('id' => $row->getId()));
    }
}
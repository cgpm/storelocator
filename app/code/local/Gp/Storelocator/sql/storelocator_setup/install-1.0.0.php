<?php
/**
 * Created by PhpStorm.
 * User: gianluca.porta
 * Date: 02/10/15
 * Time: 09:48
 */
$installer = $this;

$installer->startSetup();

$sql=<<<SQLTEXT
CREATE TABLE `gp_storelocator` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_bin NOT NULL,
  `indirizzo` varchar(255) COLLATE utf8_bin NOT NULL,
  `latitudine` varchar(255) COLLATE utf8_bin NOT NULL,
  `longitudine` varchar(255) COLLATE utf8_bin NOT NULL,
  `telefono` varchar(255) COLLATE utf8_bin NOT NULL,
  `email` varchar(255) COLLATE utf8_bin NOT NULL,
  `active` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
SQLTEXT;

$installer->run($sql);

$sql=<<<SQLTEXT
INSERT INTO `gp_storelocator` (`id`, `nome`, `indirizzo`, `latitudine`, `longitudine`, `active`)
VALUES
	(1, X'4769616E6C75636120506F727461', X'696E646972697A7A6F', X'34352E353137333831', X'392E303731393931', 1);

SQLTEXT;

$installer->run($sql);

$sql=<<<SQLTEXT
INSERT INTO `cms_page` (`title`, `root_template`, `meta_keywords`, `meta_description`, `identifier`, `content_heading`, `content`, `creation_time`, `update_time`, `is_active`, `sort_order`, `layout_update_xml`, `custom_theme`, `custom_root_template`, `custom_layout_update_xml`, `custom_theme_from`, `custom_theme_to`)
VALUES
	('Store Locator', 'store_locator_layout', NULL, NULL, 'store-locator', NULL, '<div id=\"map\">&nbsp;</div>', '2017-02-06 10:16:41', '2017-02-06 12:53:15', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL);
SQLTEXT;

$installer->run($sql);

$installer->endSetup();